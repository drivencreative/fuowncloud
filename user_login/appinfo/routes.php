<?php
/**
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */


namespace OCA\User_Login\AppInfo;


$application = new Application();

$application->registerRoutes($this, [
    'routes' => [
        /*
         * Register our only controller: Accept
         * POST requests at the URL specified, and direct
         * those requests to the create function of the class.
         *
         * We don't require anything else than this.
         */
        ['name' => 'UserLogin#tryLogin', 'url' => '/typo3redirect/{hash}', 'verb' => 'GET'],
    ]
]);
