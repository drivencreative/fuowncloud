<?php
/**
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */


namespace OCA\User_Login\AppInfo;

use \OCP\AppFramework\App;
use \OCA\User_Login\Controller\UserLoginController;

class Application extends App
{

    public function __construct(array $urlParams = array())
    {
        parent::__construct('user_login', $urlParams);

        $container = $this->getContainer();

        /*
         * Register our controller with the container.
         * Make sure that the app-name, request and database handler
         * are passed to it upon creation.
         */
        $container->registerService('UserLoginController', function ($c) {
            return new UserLoginController(
                $c->query('AppName'),
                $c->query('Request'),
                $c->query('UserManager'),
                $c->query('Config'),
                $c->query('Session'),
                $c->query('UserSession'),
                $c->query('URLGenerator')
            );
        });

        /**
         * Core class wrappers
         */
        $container->registerService('UserManager', function(SimpleContainer $c) {
            return $c->query('ServerContainer')->getUserManager();
        });
        $container->registerService('Config', function(SimpleContainer $c) {
            return $c->query('ServerContainer')->getConfig();
        });
        $container->registerService('Session', function(SimpleContainer $c) {
            return $c->query('ServerContainer')->getSession();
        });
        $container->registerService('UserSession', function(SimpleContainer $c) {
            return $c->query('ServerContainer')->getUserSession();
        });
        $container->registerService('URLGenerator', function(SimpleContainer $c) {
            return $c->query('ServerContainer')->getURLGenerator();
        });
    }
}