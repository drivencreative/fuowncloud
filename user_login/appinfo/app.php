<?php
//
//// only load text editor if the user is logged in
if (\OCP\User::isLoggedIn()) {
	$eventDispatcher = \OC::$server->getEventDispatcher();
	$eventDispatcher->addListener('OCA\Files::loadAdditionalScripts', function() {
		OCP\Util::addStyle('user_login', 'DroidSansMono/stylesheet');
		OCP\Util::addStyle('user_login', 'style');
		OCP\Util::addStyle('user_login', 'mobile');
		OCP\Util::addscript('user_login', 'editor');
		OCP\Util::addscript('user_login', 'vendor/ace/src-noconflict/ace');
	});
}

