<?php
/**
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */


namespace OCA\User_Login\Controller;

use \OC\User\Session;
use \OCP\AppFramework\Controller;
use \OCP\AppFramework\Http\RedirectResponse;
use \OCP\IConfig;
use \OCP\IRequest;
use \OCP\ISession;
use \OCP\IURLGenerator;
use \OCP\IUser;
use \OCP\IUserManager;

class UserLoginController extends Controller
{

    /**
     * Same as in Typo3 $encryptionKey
     **/
    private $encryptionKey = 'a414e6511fd084df6bc80b1ff05af415';

    /** @var IUserManager */
    private $userManager;

    /** @var IConfig */
    private $config;

    /** @var ISession */
    private $session;

    /** @var Session */
    private $userSession;

    /** @var IURLGenerator */
    private $urlGenerator;

    /**
     * @param string $appName
     * @param IRequest $request
     * @param IUserManager $userManager
     * @param IConfig $config
     * @param ISession $session
     * @param Session $userSession
     * @param IURLGenerator $urlGenerator
     */
    function __construct($appName, IRequest $request, IUserManager $userManager, IConfig $config, ISession $session, Session $userSession, IURLGenerator $urlGenerator) {
        parent::__construct($appName, $request);
        $this->userManager = $userManager;
        $this->config = $config;
        $this->session = $session;
        $this->userSession = $userSession;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @NoCSRFRequired
     * @NoAdminRequired
     * @PublicPage
     * @UseSession
     *
     * @param string $hash
     * @return RedirectResponse
     */
    public function tryLogin($hash) {
        $hash = explode(':', $this->decrypt($hash));
        $user = $hash[0];
        $password = $hash[1];
        $originalUser = $user;
        /* @var $loginResult IUser */
        $loginResult = $this->userManager->checkPassword($user, $password);
        if ($loginResult === false) {
            $users = $this->userManager->getByEmail($user);
            if (count($users) === 1) {
                $user = $users[0]->getUID();
                $loginResult = $this->userManager->checkPassword($user, $password);
            }
        }
        if ($loginResult === false) {
            $this->session->set('loginMessages', [['invalidpassword'], []]);
            $args = !is_null($user) ? ['user' => $originalUser] : [];
            return new RedirectResponse($this->urlGenerator->linkToRoute('core.login.showLoginForm', $args));
        }

        $this->userSession->login($user, $password);
        $this->userSession->createSessionToken($this->request, $loginResult->getUID(), $user, $password);

        $this->config->deleteUserValue($loginResult->getUID(), 'owncloud', 'lostpassword');

        return new RedirectResponse($this->urlGenerator->linkToRoute('files.view.index'));
    }

    /**
     * @param string $input
     * @return string
     */
    public function decrypt($input) {
        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                substr($this->encryptionKey, 0, 32),
                base64_decode(str_replace(['-', '_'], ['+', '/'], $input)),
                MCRYPT_MODE_CBC,
                substr($this->encryptionKey, 0, 16)
            ),
            "\0"
        );
    }

}
