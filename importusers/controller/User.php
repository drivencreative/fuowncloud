<?php
namespace OCA\ImportUsers\Controller;

class User extends \OC\User\User {
    /**
     * Set the password of the user
     *
     * @param string $password
     * @param string $recoveryPassword for the encryption app to reset encryption keys
     * @return bool
     */
    public function setPassword($password, $recoveryPassword = null) {
//        if ($this->emitter) {
//            $this->emitter->emit('\OC\User', 'preSetPassword', array($this, $password, $recoveryPassword));
//        }
        if ($this->backend->implementsActions(\OC\User\Backend::SET_PASSWORD)) {
            $result = $this->backend->setPassword($this->uid, $password);
//            if ($this->emitter) {
//                $this->emitter->emit('\OC\User', 'postSetPassword', array($this, $password, $recoveryPassword));
//            }
            return !($result === false);
        } else {
            return false;
        }
    }
}
