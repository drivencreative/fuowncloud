<?php
/**
 * ownCloud - importusers
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @copyright Dragan Radisic 2017
 */

namespace OCA\ImportUsers\Controller;

use \OCP\IGroupManager;
use \OCP\IRequest;
use \OCP\IUserManager;
use OCA\ImportUsers\AppInfo\Application as ImportUsers;
use \OCP\AppFramework\Controller;
use \OCP\User;

class ImportUsersController extends Controller
{
    /**
     * Same as in Owncloud $encryptionKey
     **/
    private $encryptionKey = 'a414e6511fd084df6bc80b1ff05af415';

    /** @var IUserManager */
    private $userManager;

    /** @var IGroupManager */
    private $groupManager;

    /** @var array */
    private $userGroup;

    /**
     * @param string $appName
     * @param IRequest $request
     * @param IUserManager $userManager
     */
    public function __construct($appName, IRequest $request, IUserManager $userManager, IGroupManager $groupManager)
    {
        parent::__construct($appName, $request);
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @return bool
     */
    public function createSingle()
    {
//        $this->userManager->get('pcolovic')->setPassword('peakcolovic');

        return true;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @param $id
     * @return bool
     */
    public function manageEncrypted($id)
    {
        $app = new ImportUsers();
        $feUser = $app->getUserToImport($id);
        if (!$feUser['owncloud']) return false;
        $hash = explode(':', $this->decrypt($feUser['owncloud']));
        $user = $hash[0];
        $password = $hash[1];

        $this->createUpdate($user, $password, $feUser['usergroup']);

        return true;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @return bool|\OCP\IUser
     */
    public function create()
    {
        $app = new ImportUsers();
        $feUsers = $app->getUsersToImport();
        foreach ($feUsers as $feUser) {
            $this->createUpdate($feUser['username'], $feUser['password'], $feUser['usergroup']);
        }
        return true;
    }

    /**
     * @param $userGroup
     * @return array
     */
    public function getUserGroups($userGroup)
    {
        $app = new ImportUsers();
        $feUserGroups = [];
        foreach (explode(',', $userGroup) as $gid) {
            if ($this->userGroup[$gid]) {
                $feUserGroups[$gid] = $this->userGroup[$gid];
            } else {
                $feUserGroups[$gid] = $this->userGroup[$gid] = $app->getUserGroupToImport($gid);
            }
        }
        return $feUserGroups;
    }

    /**
     * @param $user
     * @param $password
     * @param $usergroup
     * @param $recoveryPassword
     * @return \OCP\IUser|null
     */
    public function createUpdate($user, $password, $usergroup = '')
    {
        if ($this->userManager->userExists($user)) {
            /** @var $user \OC\User\User **/
            $user = $this->userManager->get($user);

            $user->setPassword($password);
//            // Encruption -> recovery key: 'PSfeuoc6!'
//            /** @var $backend \OC\User\Database **/
//            $backend = new \OC\User\Database();
//            $backend->setPassword($user->getUID(), $password);
//            \Doctrine\Common\Util\Debug::dump($user->getUID());die;
        } else {
            $user = $this->userManager->createUser($user, $password);
        }
        if ($usergroup) {
//        $groupDB = new \OC\Group\Database(\OC::$server->getDatabaseConnection());
            $qb = \OC::$server->getDatabaseConnection()->getQueryBuilder();
            $qb->delete('group_user')->where($qb->expr()->eq('uid', $qb->createNamedParameter($user->getUID())))->execute();

            /** @var $userGroup \OC\Group\Group **/
            foreach ($this->getUserGroups($usergroup) as $userGroup) {
                $this->createUpdateGroup($userGroup['gid'])->addUser($user);
            }
        }
        return $user;
    }

    /**
     * @NoAdminRequired
     * @NoCSRFRequired
     * @PublicPage
     * @return bool|\OCP\IUser
     */
    public function createGroups()
    {
        $app = new ImportUsers();
        $feUserGroups = $app->getUserGroupsToImport();
        foreach ($feUserGroups as $userGroup) {
            $this->createUpdateGroup($userGroup['gid']);
        }
        return true;
    }

    /**
     * @param $group
     * @return \OCP\IGroup
     */
    public function createUpdateGroup($group)
    {
        if ($this->groupManager->groupExists($group)) {
            $group = $this->groupManager->get($group);
        } else {
            $group = $this->groupManager->createGroup($group);
        }
        return $group;
    }

    /**
     * @param string $input
     * @return string
     */
    public function decrypt($input) {
        return rtrim(
            mcrypt_decrypt(
                MCRYPT_RIJNDAEL_128,
                substr($this->encryptionKey, 0, 32),
                base64_decode(str_replace(['-', '_'], ['+', '/'], $input)),
                MCRYPT_MODE_CBC,
                substr($this->encryptionKey, 0, 16)
            ),
            "\0"
        );
    }

}