<?php
/**
 * ownCloud - importusers
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @copyright Dragan Radisic 2017
 */

require_once __DIR__ . '/../../../tests/bootstrap.php';
require_once __DIR__ . '/../appinfo/autoload.php';