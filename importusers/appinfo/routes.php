<?php
/**
 * ownCloud - importusers
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 * @copyright Dragan Radisic 2017
 */

/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\ImportUsers\Controller\ImportUsersController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
        ['name' => 'ImportUsers#index', 'url' => '/', 'verb' => 'GET'],
        ['name' => 'ImportUsers#do_echo', 'url' => '/echo', 'verb' => 'POST'],
        ['name' => 'ImportUsers#create', 'url' => '/create', 'verb' => 'GET'],
        ['name' => 'ImportUsers#createGroups', 'url' => '/creategroups', 'verb' => 'GET'],
        ['name' => 'ImportUsers#createsingle', 'url' => '/createsingle', 'verb' => 'GET'],
        ['name' => 'ImportUsers#manageEncrypted', 'url' => '/manageEncrypted/{id}', 'verb' => 'GET']
    ]
];