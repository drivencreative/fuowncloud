<?php
/**
 * @author Dragan Radisic <dradisic@peak-sourcing.com>
 *
 * @copyright Copyright (c) 2015, ownCloud, Inc.
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 */


namespace OCA\ImportUsers\AppInfo;

use OCA\ImportUsers\Controller\ImportUsersController;
use \OCP\AppFramework\App;
use \OCA\ImportUsers\Service\DbUsers;
use PDO;
class Application extends App
{

    /** @var  \PDO */
    private $db = NULL;

    public function __construct(array $urlParams = array())
    {
        parent::__construct('importusers', $urlParams);

        $container = $this->getContainer();

        /**
         * Controllers
         */
        $container->registerService('ImportUsersController', function ($c) {
            return new ImportUsersController(
                $c->query('AppName'),
                $c->query('Request'),
                $c->query('UserManager'),
                $c->query('GroupManager')
            );
        });
        $container->registerService('UserManager', function ($c) {
            return $c->query('ServerContainer')->getUserManager();
        });

    }

    /**
     * @param $id
     * @return array
     */
    public function getUserToImport($id)
    {
        $this->db = $this->getDB();
        $stmt = $this->db->prepare("SELECT username, password, usergroup, owncloud, old_owncloud FROM fe_users WHERE uid = :uid");
        $stmt->bindParam(':uid', $id, PDO::PARAM_INT);
        $stmt->setFetchMode(PDO::FETCH_NAMED);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    /**
     * @return array
     */
    public function getUsersToImport()
    {
        $this->db = $this->getDB();
        $stmt = $this->db->prepare("SELECT username, password, usergroup, owncloud FROM fe_users");
//        SELECT fe_groups.title AS gid, fe_users.usergroup FROM fe_groups LEFT JOIN fe_users ON fe_users.usergroup REGEXP fe_groups.uid
        $stmt->setFetchMode(PDO::FETCH_NAMED);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * @param $id
     * @return array
     */
    public function getUserGroupToImport($id)
    {
        $this->db = $this->getDB();
        $stmt = $this->db->prepare("SELECT title AS gid FROM fe_groups WHERE uid = :uid");
        $stmt->bindParam(':uid', $id, PDO::PARAM_INT);
        $stmt->setFetchMode(PDO::FETCH_NAMED);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    /**
     * @return array
     */
    public function getUserGroupsToImport()
    {
        $this->db = $this->getDB();
        $stmt = $this->db->prepare("SELECT title AS gid FROM fe_groups");
        $stmt->setFetchMode(PDO::FETCH_NAMED);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    /**
     * Get the PDO database connection
     * @return string|PDO
     */
    public function getDB()
    {
        if ($this->db === null) {
            $config = [
                'DB' => [
                    'database' => 'fertilizers',
                    'host' => '127.0.0.1',
                    'password' => 'fertilizers#$ahead',
                    'port' => 3306,
                    'socket' => '',
                    'username' => 'fertilizerspeak',
                ]
            ];
            $this->db = new PDO(
                'mysql:host=' . $config['DB']['host'] . ';dbname=' . $config['DB']['database'] . ';charset=utf8;',
                $config['DB']['username'],
                $config['DB']['password'],
                [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
            );

            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $this->db;
    }
}